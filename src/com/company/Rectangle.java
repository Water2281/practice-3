package com.company;

public class Rectangle extends Shape {
    private double width;
    private double length;

    public Rectangle(){}

    public Rectangle(double width, double length){
        setWidth(width);
        setLength(length);
    }

    public Rectangle(Color c, boolean filled, double width, double length){
        super(c,filled);
        setWidth(width);
        setLength(length);
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea(){
        return width*length;
    }

    public double getPerimeter(){
        return (width+length)*2;
    }

    @Override
    public String toString() {
        return "Rectangle[" + super.toString() + ", width = " + width + ", length = " + length + "]";
    }
}
