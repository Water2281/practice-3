package com.company;

public class Main {

    public static void main(String[] args) {

        Shape circle = new Circle(Color.Green, true, 5);
        Shape rectangle = new Rectangle(Color.Red, false, 2,3);
        Shape square = new Square(Color.Yellow,true,4);

        System.out.println(circle);
        System.out.println(rectangle);
        System.out.println(square);

        System.out.println("The Area of circle = " + ((Circle) circle).getArea());
        System.out.println("The Length of circle = " + ((Circle) circle).getPerimeter());
        System.out.println("The Area of rectangle = " + ((Rectangle) rectangle).getArea());
        System.out.println("The Perimeter of rectangle = " + ((Rectangle) rectangle).getPerimeter());
        System.out.println("The Area of square = " + ((Square) square).getArea());
        System.out.println("The Perimeter of square = " + ((Square) square).getPerimeter());
    }
}
