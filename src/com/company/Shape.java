package com.company;

import org.w3c.dom.ls.LSOutput;

public class Shape {
    private Color color;
    private boolean filled;

    public Shape(){}

    public Shape(Color c,boolean bool){
        setColor(c);
        setFilled(bool);
    }

    public void setColor(Color color){
        this.color = color;
    }

    public Color getColor(){
        return color;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public boolean isFilled() {
        return filled;
    }

    @Override
    public String toString() {
        return "Shape[color = " + color + ", " + "filled = " + filled + "]";
    }
}
