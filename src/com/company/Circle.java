package com.company;

public class Circle extends Shape {
    private double radius;

    public Circle(){}

    public Circle(double radius){
        setRadius(radius);
    }

    public Circle(Color c, boolean filled, double radius){
        super(c,filled);
        setRadius(radius);
    }

    public double getRadius(){
        return radius;
    }

    public void setRadius(double radius){
        this.radius = radius;
    }

    public double getPerimeter(){
        return 2*3.14*radius;
    }

    public double getArea(){
        return 3.14*Math.pow(radius,2);
    }

    @Override
    public String toString() {
        return "Circle[" + super.toString() + ", radius = " + radius + "]";
    }
}
