package com.company;

public enum Color {
    Black,
    Green,
    Orange,
    Purple,
    Red,
    White,
    Yellow,
    Blue,
    Aqua,
    Grey,
    Pink,
}
