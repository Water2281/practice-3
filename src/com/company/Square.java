package com.company;

public class Square extends Rectangle {
    public Square(){}

    public Square(double side){
        super(side,side);
    }

    public Square(Color c, boolean filled,double side){
        super(c,filled,side,side);
    }

    public double getSide(){
        return getWidth();
    }

    public void setSide(double side){
        setWidth(side);
        setLength(side);
    }

    @Override
    public String toString() {
        return "Square[" + super.toString() + "]";
    }
}
